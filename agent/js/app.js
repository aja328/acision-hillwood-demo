$(document).ready(function(e) {
	var sdk;
	//webrtc session
	var session;	
	//local media
	var local;
	//remote media
	var remote;	
    var agent;
	var conversationSubject = '';    
	
	//enter api key form your Acision Forge account	
	var key = 'UbVEfPPqHxFb';
	var url	= $.url();
	
	var guestUser = {
		username: 'aja_slalom_com_0',
		name: 'Guest'
	};	
	
	//setup calendar
    $('#calendar').fullCalendar({
		events: [ {title: 'Appt with John', start: '2015-08-06'},
    			  {title: 'Appt with Steve', start: '2015-08-14'}
				]
    });	
  
    var agent1 = {
    	name: 'Christine',
    	username: 'aja_slalom_com_1',
    	pw: '9Ie6Cc0@n',
    	image: '/monterra/images/Adeeb_Christine80.jpg'
    };

    var agent2 = {
    	name: 'Stacy',
    	username: 'aja_slalom_com_2',
    	pw: 'Hz08ViYZo',
    	image: '/monterra/images/Heffernan_Stacy80.jpg'
    };
	
	//get user id
	if($.url().param('uid') == '1' || $.url().param('uid') == null) {
		agent = agent1;
	} else if($.url().param('uid') == '2'){
		agent = agent2;
	};
	
	setTimeout(function(){
		sdk = new AcisionSDK(key, {
			onConnected		: onConnected,
			onDisconnected	: onDisconnected
		},{
			persistent	: true,
			username	: agent.username,
			password	: agent.pw
		});
	},500);
	
	//on connection
	function onConnected(evt){			
		//on incoming session
		sdk.webrtc.setCallbacks({
			onIncomingSession: onCall
		});	
		
		sdk.messaging.setCallbacks({
	    	onMessage: acisionSDK_onMessage
	    });		
	    
		//get customer presence
		sdk.presence.getPresentities([guestUser.username + '@sdk.acision.com'], null, {
			onSuccess: function(evt){						
				if(evt[0].fields.status == 'available'){
					$(".customer.presence").addClass("online").removeClass("offline");
				}
			}
		});	
		//get current agent presence
		sdk.presence.getPresentities([agent.username + '@sdk.acision.com'], null, {
			onSuccess: function(evt){						
				if(evt[0].fields.status == 'available'){	
					//set current status										
					$('#availability span').text('OFFLINE');
					$(".agent.presence").addClass("online").removeClass("offline");
					$('#agent-status').fadeIn();									
				} else {			
					$('#agent-status').fadeIn();								
				};
			}
		});		
		
		//subscribe to customer presence updates
		sdk.presence.subscribe([guestUser.username + '@sdk.acision.com'], ['status', 'name', 'email', 'phone']);		    
		
		//on customer presence updates
		sdk.presence.setCallbacks({
			onPresentity : function(evt){
				var user 	= evt[0].user;
				var status 	= evt[0].fields.status;
				var name 	= evt[0].fields.name;
				var email 	= evt[0].fields.email;
				var phone 	= evt[0].fields.phone;
				
				if (name != null & name !== '') {
					$("#customerNameSpan").text(name);
					$("#customerEmailSpan").text(email);
					$("#customerPhoneSpan").text(phone);
					$("#customerInfo").removeClass("hide");
					guestUser.name = name;		
				    conversationSubject = 'Name: ' + name + ', Email: ' + email +
	    				', Phone: ' + phone;
				}
						
				if(status == 'available'){
					$(".customer.presence").addClass("online").removeClass("offline");					
				} else if(status == null || '' === status || status == 'unavailable'){
					$(".customer.presence").addClass("offline").removeClass("online");
					$("#customerInfo").addClass("hide");					
				}
				resizeChat();
			}
		});		
		
	    $("#dlgChat").dialog("option", "title", "Live Chat Powered by Slalom & Acision").dialog("open");
	};
	
	//on disconnect
	function onDisconnected(evt){
		$('#disconnect').hide();
		$('#accept').hide();	
			
		//clean html elements
		$('#videoLocal').attr('src', '');	
		$('#videoRemote').attr('src', '');
			
		//set presence 
		sdk.presence.setOwnPresentity({
			status : 'unavailable'
		});
			
		//set current status
		$('#availability span').text('ONLINE');
		$(".agent.presence").addClass("offline").removeClass("online");
			
		//define new sdk
		sdk = new AcisionSDK(key, {
			onConnected		: onConnected,
			onDisconnected	: onDisconnected
		},{
			username	: agent.username,
			password	: agent.pw
		});			
	};

	//incoming call
	function onCall(evt){
	    $("#dlgChat").dialog("close");
		$('#calendar-div').addClass('callActive');
		$('#notepad').addClass('callActive');
		session = evt.session;
			
		//remote video & audio render
		session.remoteAudioElement 	= document.getElementById('audioRemote');
		session.remoteVideoElement 	= document.getElementById('videoRemote');
		session.localVideoElement = document.getElementById('videoLocal');
				
		//session ended callback
		session.setCallbacks({
			onClose: onCallClose
		});

		acceptCall();
	};
	
	function acceptCall(){			
		$('#accept').fadeIn();
		$('.loading').fadeIn();
		document.getElementById('ringtone').play();
			
		$('#accept').click(function(){		
			session.accept();
			$('#accept').hide();
			$('.loading').hide();	
					
			//show local
			$('.local').fadeIn();	
			$('#disconnect').fadeIn();
			document.getElementById('ringtone').pause();		

			sdk.presence.setOwnPresentity({
				status : 'unavailable'
			});	
					
			//set current status
			$('#availability span').text('ONLINE');
			$(".agent.presence").addClass("offline").removeClass("online");
			$('#availability').hide();
		});			
	};
		
	$('#disconnect').click(function(){			
		sdk.disconnect();
		$('.local').hide();
		$('#calendar-div').removeClass('callActive');
		$('#notepad').removeClass('callActive');		
		$('#availability').show();			
	});
	
	//windown unload
	window.onunload = window.onbeforeonload = (function(){
		sdk.disconnect();
	});
	
	$(window).resize(function() {
		resizeChat();
	});	
	
	//set availability
	$('#availability').click(function(){
		//check availability
		if($('#availability span').text() == 'OFFLINE'){			
			//set current status
			$('#availability span').text('ONLINE');					
			$(".agent.presence").addClass("offline").removeClass("online");
		
			//set presence
			sdk.presence.setOwnPresentity({
				status : 'unavailable'
			}, '', { 
				onSuccess: function(){}
			});
		} else if($('#availability span').text() == 'ONLINE'){
			$('#availability span').text('OFFLINE');
			$(".agent.presence").addClass("online").removeClass("offline");
	
			//set presence
			sdk.presence.setOwnPresentity({
				status : 'available'
			}, '', { 
				onSuccess: function(){}				
			});	
		};	
	});
	
	$.widget("ui.dialog", $.ui.dialog, {
		_title: function (title) {
	      // Override title function to insert the Acision logo (this is
    	  // is needed as the default function escapes HTML)
	      title.html("<img src='image/slalom-s.png' width='16' height='16' style='margin-right: 0.5em; vertical-align: bottom;'>"
      		  + "<img src='image/forge-icon.png' width='16' height='16' style='margin-right: 0.5em; vertical-align: bottom;'>"
              + this.options.title);
    	}
	});
	
    $("#dlgChat").dialog({
  	 	autoOpen: false,
	    closeOnEscape: false,
    	hide: false,
	    show: true,
    	width: 400,
		position:  { my: "center+5%", at: "right+55%", of: "#calendar" },
	    buttons: [
	      {text: "Request Video", click: dlgVideoChat}
    	]		
	});

	function dlgVideoChat(e) {
		//connect to agent
		session = sdk.webrtc.connect(guestUser.username, {
			onConnect		: onCallConnect,
			onConnecting	: onCallConnecting,
			onClose			: onCallClose
		},{
			streamConfig: {
			  audioIn	: true,
			  audioOut	: true,
			  videoIn	: true,
			  videoOut	: true
			}
		});

		session.localVideoElement = document.getElementById('videoLocal'); 
		session.remoteAudioElement 	= document.getElementById('audioRemote');
		session.remoteVideoElement 	= document.getElementById('videoRemote');
	}
	
	function onCallConnect(){
		//notify user that the call has been accepted and will start shortly
		$('#outgoing').hide();
		$('#disconnect').fadeIn();
	};
	
	function onCallConnecting(){			
	    $("#dlgChat").dialog("close");
		$('#calendar-div').addClass('callActive');
		$('#notepad').addClass('callActive');
		$('#outgoing').fadeIn();
	};
	
	function onCallClose(evt){
		//clean remote html element
		$('#videoLocal').attr('src', '');	
		$('#videoRemote').attr('src', '');
			
		//pause ringing
		document.getElementById('ringtone').pause();

		$('#calendar-div').removeClass('callActive');
		$('#notepad').removeClass('callActive');						
		$('.local').hide();
				
		//disconnect from session
		setTimeout(function(){
			sdk.disconnect();	
		},1000);
				
		//show go online button	
		$('#availability').show();
			
		//reload page	
		setTimeout(function(){
			location.reload();
		},3000);		
	};			
	
	function acisionSDK_onMessage(msg) {
	  console.log("acisionSDK_onMessage()");
  	  console.info("Received message " + msg.content);

	  // Force a reload of the audio otherwise Chrome will only ever play the file once
	  document.getElementById("audDing").load();
	  document.getElementById("audDing").play();
	  addChatLine(guestUser.name, msg.content);
	}
	
	function addChatLine(orig, message) {
	  divLine = $(document.createElement("div"));
	  spnOrig = $(document.createElement("span"));
	  spnMessage = $(document.createElement("span"));
	  spnOrig.addClass("spnOrig");
	  spnMessage.addClass("spnMessage");
	  spnOrig.text(orig + " (" + (new Date()).toLocaleTimeString() + "):");
	  spnMessage.text(message);
	  divLine.append(spnOrig).append(spnMessage);
	  $("#divChatLog").append(divLine).scrollTop($("#divChatLog").prop("scrollHeight"));
	}	
	
    function btnSend_click(event) {
	  var sMsg = $("#txtMessage").val();
	  $("#txtMessage").val("");

	  console.info("Sending message " + sMsg);
	  var options = {
	  		subject: conversationSubject
	  };

	  sdk.messaging.sendToDestination(guestUser.username, sMsg, options)
	  addChatLine(agent.name, sMsg);
	}
	
	function resizeChat() {
		$("#dlgChat").dialog( "option", "position", { my: "center+5%", at: "right+55%", of: "#calendar" });	
	}
	
	
    $("#txtMessage").keypress(handleEnter(btnSend_click));	
    $("#btnSend").button().click(btnSend_click);	
	$('#agent-image').attr('src', agent.image);
});