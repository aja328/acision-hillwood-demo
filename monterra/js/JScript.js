// JScript File
// Original Author: hoangnx.fpt@gmail.com
// Extensively modified by DaoA
// Granted DNN360.net for use 
//-----------------------------------------------

image1 = new Image();
image1.src = commonfolder+"/spinner4.gif";

image2 = new Image();
image2.src = commonfolder+"/loading.gif";

image3 = new Image();
image3.src = commonfolder+"/spinner.gif";

spinner=commonfolder+"/spinner.gif";
function FindObjectByID(idName)
{
	return document.getElementById(idName);
}
function UnselectedClass(classname)
{
	 	
	var strDivName ="";
	var i=classname.indexOf("_");
 	
	if (i>=0)
		{
		strDivName = classname.substring(0,classname.indexOf("_"));
		return strDivName ;
		}
		else
		{strDivName =classname;}
		
	
	return 	strDivName ;
}
//-----------------------------------------------
function SetBlockTab(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		spinner="spinner5red.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		var stri = strDivName;
		
        for (var i = 1; i <= intTabLength; i++)
        {
				
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
			
            FindObjectByID(stri).className = unselect_class;
			FindObjectByID("L"+stri).className = "left";
			FindObjectByID("R"+stri).className = "right";
			
            FindObjectByID(FindObjectByID(stri).headers).style.display = "none";
        }
        FindObjectByID(strName).className = select_class;
		FindObjectByID("L"+strName).className = "left-on";
		FindObjectByID("R"+strName).className = "right-on";
		
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		//alert(strName);
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
    }
    catch(ex){}
}
function SelectedClass(classname)
{
	 	
	var strDivName ="";
	var i=classname.indexOf("_");
 	
	if (i>=0)
		{
		return classname;
		}
		else
		{strDivName =classname+"_selected";}
	return 	strDivName ;
}
//-------------------------------------------------------
function SetBlockColor(strName,intTabLength, intStart,isAjaxTab,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		//alert(strDivName1 );
		var stri = strDivName;
		var _class="";
        for (var i = 1; i <= intTabLength; i++)
        {
            
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
			
			var _class=FindObjectByID(stri).className ;
		
            FindObjectByID(stri).className = UnselectedClass(_class);
			
            FindObjectByID(FindObjectByID(stri).headers).style.display = "none";
        }
		_class=FindObjectByID(strName).className ;
        FindObjectByID(strName).className = SelectedClass(_class);
		
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		//alert(strName);
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
    }
    catch(ex){}
}
//-----------------------------------------------
function SetBlockWebClientYAanswer(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,media_url,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		spinner="spinnerbig.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		
		var stri = strDivName;
		
        for (var i = 1; i <= intTabLength; i++)
        {
            
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
			//alert(stri);
            var id="YAleft"+stri;
			 
			FindObjectByID(id).src= media_url+"left.gif";
			id="YAright"+stri;
			FindObjectByID(id).src= media_url+"right.gif";
			id="YAcenter"+stri;
			//alert(FindObjectByID(id).style);
			//debugger;
			FindObjectByID(id).style.backgroundImage=  "url("+media_url+"repeat.gif)" ;
			FindObjectByID(id).className = unselect_class;
		    FindObjectByID(FindObjectByID(stri).headers).style.display = "none";
        }
		//alert(select_class)
       //alert(module_url);
		//FindObjectByID("TABIMG"+strName).src= module_url+"Arrow.gif";
		    var id1="YAleft"+strName;
			 
			FindObjectByID(id1).src= media_url+"selectedleft.gif";
			id1="YAright"+strName;
			FindObjectByID(id1).src= media_url+"selectedright.gif";
			id1="YAcenter"+strName;
			FindObjectByID(id1).style.backgroundImage=  "url("+media_url+"selectedrepeat.gif)" ;
			FindObjectByID(id1).className = select_class;
		
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		//alert(strName);
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
    }
    catch(ex){}
}

//-----------------------------------------------
function SetBlockWebClientArrow(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		spinner="loading.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		
		var stri = strDivName;
		
        for (var i = 1; i <= intTabLength; i++)
        {
            
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
		    
            var id="TABIMG"+stri;
			//alert(id);
			//alert("url('"+module_url+"'Arrow.gif')  no-repeat top center;" );
			FindObjectByID(id).className=""; 
			FindObjectByID(stri).className = unselect_class;
			
            FindObjectByID(FindObjectByID(stri).headers).style.display = "none";
        }
		// alert(strName)
        FindObjectByID(strName).className = select_class;
		//alert(module_url);
		//alert("TABIMG"+strName);
		//alert("url('"+module_url+"Arrow.gif')  no-repeat top center;")
		FindObjectByID("TABIMG"+strName).className="ArrowSelected";
		//	alert(FindObjectByID("TABIMG"+strName).style.background);
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		//alert(strName);
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
    }
    catch(ex){}
}

//-----------------------------------------------
function SetBlockWebClientJquery(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,module_url)
{
    try
    {
      
		 
		spinner="loading.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		
		var stri = strDivName;
		 
		 
		//--------------------now change the content------------------------------
		 
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		 
		 
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		 
      
    }
    catch(ex){}
}

function SetBlockWebClientYMV(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		spinner= commonfolder+"/spinner5.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		
		var stri = strDivName;
        for (var i = 1; i <= intTabLength; i++)
        {
            
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
			//	alert(stri);
            FindObjectByID(stri+"Head").className = "";
            FindObjectByID(stri).style.opacity= "0";
			FindObjectByID(stri).style.display= "none";
        }
		FindObjectByID(strName+"Head").className =  "on";
     //   FindObjectByID(strName).className = select_class;
		
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		 
		 FindObjectByID(strName).style.opacity = "1";
		 FindObjectByID(strName).style.display= "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		{
		 FindObjectByID(strName).style.opacity = "1";
		 FindObjectByID(strName).style.display= "block";
		 }
    }
    catch(ex){}
}
function SetBlockWebClientYm(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		spinner= commonfolder+"/loading.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		 
	
		var stri = strDivName;
        for (var i = 1; i <= intTabLength; i++)
        {
            
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
			//	alert(stri);
            FindObjectByID(stri+"Head").className = unselect_class;
            FindObjectByID(stri).style.display = "none";
        }
		FindObjectByID(strName+"Head").className =  select_class;
     //   FindObjectByID(strName).className = select_class;
		
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		 
		 FindObjectByID(strName) .style.display = "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		 FindObjectByID(strName).style.display = "block";
    }
    catch(ex){}
}
//-----------------------------------------------SetBlockWebClient
function SetBlockWebClient(strName,intTabLength, intStart,unselect_class,select_class,isAjaxTab,module_url)
{
    try
    {
        //var strDivName = strName.substring(0,strName.length-2);
		
		spinner= commonfolder+"/spinner4.gif";
		var strDivName = strName.substring(0,strName.indexOf("_"));
		document.cookie = strDivName+" = " + strName;
		
		var stri = strDivName;
        for (var i = 1; i <= intTabLength; i++)
        {
            
            var temp = i + intStart;
            if (temp < 10) 
                stri = strDivName+'_'+temp;
            else 
                stri = strDivName+'_'+temp;
			//	alert(stri);
            FindObjectByID(stri).className = unselect_class;
            FindObjectByID(FindObjectByID(stri).headers).style.display = "none";
        }
        FindObjectByID(strName).className = select_class;
		
		//--------------------now change the content------------------------------
		if (isAjaxTab=="1")
		{
		var path=module_url;
		var url = "RequestHandler.aspx?Divname=" + strName;
		//alert(strName);
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
		var contentdiv="myAjaxDiv_"+strName;
		document.getElementById(contentdiv).innerHTML =contentdiv;
		postRequest(path,url,contentdiv);	
		}
        else 
		FindObjectByID(FindObjectByID(strName).headers).style.display = "block";
    }
    catch(ex){}
}
//------------------------------------------------------------------
//this function from http://www.peej.co.uk/articles/rich-user-experience.html
				function getHTTPObject() {
				if (typeof XMLHttpRequest != 'undefined') { 
				return new XMLHttpRequest(); 
				} 
				try {
				return new ActiveXObject("Msxml2.XMLHTTP"); 
				} catch (e) { 
				   try { 
				   return new ActiveXObject("Microsoft.XMLHTTP"); 
				   } catch (e) {}
				} 
				return false; 
				}
				/*
					Creates a XMLHTTP async-call to an ASP.NET async handler
				*/
				function postRequest(path,url,whichdiv)
				{
				var xmlhttp=getHTTPObject();
				xmlhttp.open("GET", path+url, true);
				// Register a callback for the call
					xmlhttp.onreadystatechange = 
						function ()
						{
				 	 	if (xmlhttp.readyState == 4)
							{
								var response = xmlhttp.responseText;
								document.getElementById(whichdiv).innerHTML = response ;
								
								if(xmlhttp.status == 200)
									return true;
									else
									document.getElementById(whichdiv).innerHTML = "Error retrieving XML data..!";
								
							}
							else
							{	
							
							    document.getElementById(whichdiv).innerHTML = '<div align="center"><img alt="" src="'+path+spinner+'" vspace="10" /></div><br/>';
								
							}
  						}
					
					// Send the actual request
					xmlhttp.send(null);
					
				}
				
				/*
					Clears the response div
				*/
				function clearResponse()
				{
					divResponse.innerHTML = "";
				}
			//-->
 
 //------------------------------------------------------------------------------------------------------------------------
 /* Client-side access to querystring name=value pairs
	Version 1.2.3
	22 Jun 2005
	Adam Vandenberg
*/
function Querystring(qs) { // optionally pass a querystring to parse
	this.params = new Object()
	this.get=Querystring_get
	
	if (qs == null)
		qs=location.search.substring(1,location.search.length)

	if (qs.length == 0) return

// Turn <plus> back to <space>
// See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
	//alert("ok");
	qs = qs.replace(/\+/g, ' ')
	var args = qs.split('&') // parse out name/value pairs separated via &

	//alert(args.length);
	
// split out each name=value pair
	for (var i=0;i<args.length;i++) {
		var value;
		var pair = args[i].split('=')
		var name = unescape(pair[0])

		if (pair.length == 2)
			value = unescape(pair[1])
		else
			value = name
		
		this.params[name] = value
	}
}

function Querystring_get(key, default_) {
	// This silly looking line changes UNDEFINED to NULL
	if (default_ == null) default_ = null;
	
	var value=this.params[key]
	if (value==null) value=default_;
	
	return value
}
//-----------------HANDLE ONLOAD----------------------------------------------------------------
/*
 * (c)2006 Dean Edwards/Matthias Miller/John Resig
 * Special thanks to Dan Webb's domready.js Prototype extension
 * and Simon Willison's addLoadEvent
 *
 * For more info, see:
 * http://dean.edwards.name/weblog/2006/06/again/
 * http://www.vivabit.com/bollocks/2006/06/21/a-dom-ready-extension-for-prototype
 * http://simon.incutio.com/archive/2004/05/26/addLoadEvent
 
 *
 */
 
function addDOMLoadEvent(func) {
   if (!window.__load_events) {
      var init = function () {
          // quit if this function has already been called
          if (arguments.callee.done) return;
      
          // flag this function so we don't do the same thing twice
          arguments.callee.done = true;
      
          // kill the timer
          if (window.__load_timer) {
              clearInterval(window.__load_timer);
              window.__load_timer = null;
          }
          
          // execute each function in the stack in the order they were added
          for (var i=0;i < window.__load_events.length;i++) {
              window.__load_events[i]();
          }
          window.__load_events = null;
      };
   
      // for Mozilla/Opera9
      if (document.addEventListener) {
          document.addEventListener("DOMContentLoaded", init, false);
      }
      
      // for Internet Explorer
      /*@cc_on @*/
      /*@if (@_win32)
          document.write("<scr"+"ipt id=__ie_onload defer src=//0><\/scr"+"ipt>");
          var script = document.getElementById("__ie_onload");
          script.onreadystatechange = function() {
              if (this.readyState == "complete") {
                  init(); // call the onload handler
              }
          };
      /*@end @*/
      
      // for Safari
      if (/WebKit/i.test(navigator.userAgent)) { // sniff
          window.__load_timer = setInterval(function() {
              if (/loaded|complete/.test(document.readyState)) {
                  init(); // call the onload handler
              }
          }, 10);
      }
      
      // for other browsers
      window.onload = init;
      
      // create event function stack
      window.__load_events = [];
   }
   
   // add function to event stack
   window.__load_events.push(func);
}
// get Cookie

function getCookieA(name1) {
	var name = name1.substring(0,name1.indexOf("_"));
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}