/**
 * Show/hide a busy spinner in the button pane of a dialog.
 *
 * @param dialog
 *       the JQuery selector of the dialog to modify
 * @param show
 *       true to show the spinner or false to hide it
 */
function showSpinner(dialog, show) {
  if (show) {
    $(dialog).dialog("widget").find(".ui-dialog-buttonset").prepend(
   	    $("#imgSpinner").removeClass("ui-helper-hidden"));
  } else {
    $("body").append($("#imgSpinner").addClass("ui-helper-hidden"));
  }
}	
	
/**
 * Enable/disable a dialog by setting the disabled attribute on the dialog
 * buttons.
 *
 * @param dialog
 *       the JQuery selector of the dialog to modify
 * @param enable
 *       true to enable the dialog or false to disable it
 */
function enableDialog(dialog, enable) {
  button = $(dialog).dialog("widget").find("button");
  if (enable) {
    button.removeAttr("disabled").removeClass("ui-state-disabled");
  } else {
    button.attr("disabled", "disabled").addClass("ui-state-disabled");
  }
}	
	
/**
 * Handle the enter key in a text box by running the provided submitFunction.
 *
 * @param submitFunction
 *       the event handler to run when the Enter key is pressed
 * @return a keypress event handler
 */
function handleEnter(submitFunction) {
  return function(event) {
   	if (event.keyCode == 13) {
      submitFunction();
   	  event.preventDefault();
    }
  }
}