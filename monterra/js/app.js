$(document).ready(function(e) {
    var sdk;
	var key = 'UbVEfPPqHxFb';
	
	//login as customer user account
	var guestUser = {
		username: 'aja_slalom_com_0',
		pw: 'fILTEAAjh',
		name: 'Guest'
	};
	var agent1 = {
		name: 'Christine',
		username: 'aja_slalom_com_1'
	};
	var agent2 = {
		name: 'Stacy',
		username: 'aja_slalom_com_2'
	};	
	
	//agent	
	var agent;
	var agentName;
	var agentTitle;
	var agentImage;
	
	//webrtc session
	var session;
	//local media
	var local;
	//agent media
	var remote;
	var isRecording = false;
	var conversationSubject = '';
	
	//modals
	var agent_pin_number;
	var agent_notice;	
	
	setupAppointmentBehaviors();

	//define sdk
	setTimeout(function(){			
		sdk = new AcisionSDK(key, {
			onConnected		: onConnected,
			onDisconnected	: onDisconnected,
			onAuthFailure	: onAuthFailure
		},{
			persistent	: true,
			username	: guestUser.username,
			password	: guestUser.pw
		});
	},500);
	
	//start agent chat
	//OUTGOING CHAT
	$('.call').click(function(e){	
		e.preventDefault();
			
		//get remote user id
		agent = $(this).data('agent-id');
		agentName = $(this).data('agent-name');
		agentImage = $(this).data('agent-image');
		agentTitle = $(this).data('agent-title');
		
		sdk.messaging.setCallbacks({
	    	onMessage: acisionSDK_onMessage
	    });
	    
	    //scroll to top of page
	    window.scrollTo(0,0);
    	$("#dlgPreChat").dialog("option", "title", "Connecting with " + agentName).dialog("open");
		$('#chat-container').show();
		$('#page-container').hide();
	});
	
	//incoming call
	function onCall(evt){
		//start session
		session = evt.session;
		
		$('.video .loading').show();
    	$("#dlgChat").dialog("close");
			
		//remote video & audio render
		session.remoteAudioElement 	= document.getElementById('audioRemote');
		session.remoteVideoElement 	= document.getElementById('videoRemote');
			
		//local video render
		session.localVideoElement = document.getElementById('videoLocal');				
		$('video').prop('autoplay', true);

		session.setCallbacks({
			onClose: onCallClose
		});

		acceptCall();
	};
	
	function acceptCall(){		
		session.accept();
		//hide accept button
		$('.video .loading').hide();
		$('#enter-agent-pin .loading').hide();	
		
		$('.video-chat').fadeIn();	
		//show waiting messaging
		$('.video .loading').show();
		$('#page-container').hide();		
	};	

	//ON CONNECTION TO SDK
	function onConnected(evt){
		sdk.webrtc.setCallbacks({
			onIncomingSession: onCall
		});			
			
		//set presence
		sdk.presence.setOwnPresentity({
				status : 'available'
			}, '', {}
		);

		//get agent one presence
		sdk.presence.getPresentities([agent1.username + '@sdk.acision.com'], null, {
			onSuccess: function(evt){						
				if(evt[0].fields.status == 'available'){
					$("#agent-one .chatBtn span").text("CHAT NOW").parent().addClass('online');
				};							
			}
		});

		//get agent two presence
		sdk.presence.getPresentities([agent2.username + '@sdk.acision.com'], null, {
			onSuccess: function(evt){
				if(evt[0].fields.status == 'available'){
					$("#agent-two .chatBtn span").text("CHAT NOW").parent().addClass('online');
				};							
			}
		});
			
		//subscribe to persences
		sdk.presence.subscribe([agent1.username + '@sdk.acision.com',agent2.username + '@sdk.acision.com'], ['status']);
				
		//subscribe to presence
		sdk.presence.setCallbacks({
			onPresentity : function(evt){						
				//variables
				var user 	= evt[0].user;
				var status 	= evt[0].fields.status;	
						
				//check users
				if(user == agent1.username + '@sdk.acision.com'){
					if(status == 'available'){
						$("#agent-one .chatBtn span").text("CHAT NOW");
						$("#agent-one .chatBtn span").parent().addClass('online').removeClass('offline');
					} else if(status == null || '' === status || status == 'unavailable'){
						$("#agent-one .chatBtn span").text("OFFLINE");
						$("#agent-one .chatBtn span").parent().addClass('offline').removeClass('online');
					}				
				} else if(user == agent2.username + '@sdk.acision.com'){
					if(status == 'available'){			
						$("#agent-two .chatBtn span").text("CHAT NOW");
						$("#agent-two .chatBtn span").parent().addClass('online').removeClass('offline');
					} else if(status == null || '' === status || status == 'unavailable'){
						$("#agent-two .chatBtn span").text("OFFLINE").parent().addClass('offline');
						$("#agent-two .chatBtn span").parent().addClass('offline').removeClass('online');
					}				
				}		
			}
		});
				
		$('video').prop('autoplay', true); 	
	};
	
	function disconnect() {
		if (isRecording) {
		    sdk.messaging.stopRecordingConversation(agent + '@sdk.acision.com',
		    	conversationSubject);
		}
		//delete presence
		sdk.presence.deleteOwnPresentity();			
		sdk.disconnect();	
	}

	//ON DISCONNECT FROM SDK
	function onDisconnected(evt){
		$('#videoLocal').attr('src', '');	
		$('#videoRemote').attr('src', '');
		$('#disconnect').hide();		
			
		//define new sdk
		sdk = new AcisionSDK(key, {
			onConnected		: onConnected,
			onDisconnected	: onDisconnected,
			onAuthFailure	: onAuthFailure
		},{
			username: guestUser.username,
			password: guestUser.pw
		});			
	};

	$('.hangup').click(function(){
		disconnect();
		//show waiting messaging
		$('.video .loading').show();
		//hide agent app
		$('.video-chat').hide();
		$('#chat-container').hide();		
		//show demo app page	
		$('#page-container').fadeIn();
	});
	
	$.widget("ui.dialog", $.ui.dialog, {
		_title: function (title) {
	      // Override title function to insert the Acision logo (this is
    	  // is needed as the default function escapes HTML)
	      title.html("<img src='images/slalom-s.png' width='16' height='16' style='margin-right: 0.5em; vertical-align: bottom;'>"
      		  + "<img src='images/forge-icon.png' width='16' height='16' style='margin-right: 0.5em; vertical-align: bottom;'>"
              + this.options.title);
    	}
	});	
	  
	$("#dlgPreChat").dialog({
    	autoOpen: false,
	    closeOnEscape: false,
    	hide: true,
	    show: true,
    	width: 450,
	    close: dlgPreChat_beforeClose,    	
	   	position: { my: "center", at: "center", of: window },
    	buttons: [
	      {text: "Start chat", click: dlgPreChat_click}
    	]
	});
	
	$("#dlgChat").dialog({
    	autoOpen: false,
	    closeOnEscape: false,
    	hide: true,
	    show: true,
    	width: 450,
	    close: dlgChat_beforeClose,
	    buttons: [
	      {text: "Request Video", click: dlgVideoChat}
    	]
	});	

	function dlgVideoChat(e) {
		//connect to agent
		session = sdk.webrtc.connect(agent, {
			onConnect		: onCallConnect,
			onConnecting	: onCallConnecting,
			onClose			: onCallClose
		},{
			streamConfig: {
			  audioIn	: true,
			  audioOut	: true,
			  videoIn	: true,
			  videoOut	: true
			}
		});

		session.localVideoElement = document.getElementById('videoLocal'); 
		session.remoteAudioElement 	= document.getElementById('audioRemote');
		session.remoteVideoElement 	= document.getElementById('videoRemote');
	}
	
	function onCallConnect(){
		//notify user that the call has been accepted and will start shortly
		$('.video .loading').hide();
		$('#enter-agent-pin .loading').hide();
		$('#chat-container').hide();
	};
	
	function onCallConnecting(){			
		//hide all modals
	    $(".ui-dialog-content").dialog("close");		
		//show agent app
		$('.video-chat').fadeIn();	
		//show waiting messaging
		$('.video .loading').show();
		$('#page-container').hide();
	};
		
	function onCallClose(evt){
		//get on call close status
		var status = evt.status;
							
		//clean remote html element
		$('#videoLocal').attr('src', '');	
		$('#videoRemote').attr('src', '');
			
		//show waiting messaging
		$('.video .loading').show();

		//hide agent app
		$('.video-chat').hide();
		$('#chat-container').hide();
		//show demo app page	
		$('#page-container').fadeIn();
	    $(".ui-dialog-content").dialog("close");		

		//offline agent
		if(status == 'offline'){
			//show modal
			agent_notice = $('#agent-offline').bPopup();
			//hide loading
			$('#enter-agent-pin .loading').hide();						
		};			
	};	
  
    function dlgPreChat_click(event) {
        var name = $("#customerName").val();
        var phone = $("#customerPhone").val();
        var email = $("#customerEmail").val();
        guestUser.name = name;
        
        if (isValidPreChat(name, email, phone)) {
	        sdk.presence.setOwnPresentity({
	  		  status : 'available',
			  name: name,
			  phone: phone,
			  email: email
			});
        
    	    $("#customerNameSpan").text($("#customerName").val());
	        $("#customerPhoneSpan").text($("#customerPhone").val());
    	    $("#customerEmailSpan").text($("#customerEmail").val());    
        	$("#agentImage").attr("src", agentImage);
	        $("#agentName").text(agentName);
    	    $("#agentTitle").text(agentTitle);        
		   	$("#dlgChat").dialog("option", "title", "Live Chat Powered by Slalom & Acision").dialog("open");
			$("#dlgPreChat").dialog("close");
        	$("#errorMessage").addClass("hide");
        	
		    conversationSubject = 'Name: ' + name + ', Email: ' + email +
	    		', Phone: ' + phone;
		    sdk.messaging.startRecordingConversation(agent + '@sdk.acision.com', 
	    		conversationSubject);
		    isRecording = true;        		   	
        } else {
        	$("#errorMessage").removeClass("hide");
        }
    }
  
	function dlgPreChat_beforeClose(event, ui) {
	  console.log("dlgChat_beforeclose()");
	  // If next chat dialog hasn't been opened by dlgPreChat_click
	  // then revert to default page state
	  if (!$("#dlgChat").dialog("isOpen")) {
		$('#chat-container').hide();	  
      	$('#page-container').show();
      }
	}

	function dlgChat_beforeClose(event, ui) {
	  console.log("dlgChat_beforeclose()");
	  if (session == null) {
		$('#chat-container').hide();
      	$('#page-container').show();
      }
	}	
	
	function btnSend_click(event) {
	  var sMsg = $("#txtMessage").val();
	  $("#txtMessage").val("");

	  console.info("Sending message " + sMsg);
	  var options = {
	  		subject: conversationSubject
	  };

	  sdk.messaging.sendToDestination(agent, sMsg, options);
	  addChatLine(guestUser.name, sMsg);
	}	
	
	function addChatLine(orig, message) {
	  divLine = $(document.createElement("div"));
	  spnOrig = $(document.createElement("span"));
	  spnMessage = $(document.createElement("span"));
	  spnOrig.addClass("spnOrig");
	  spnMessage.addClass("spnMessage");
	  spnOrig.text(orig + " (" + (new Date()).toLocaleTimeString() + "):");
	  spnMessage.text(message);
	  divLine.append(spnOrig).append(spnMessage);
	  $("#divChatLog").append(divLine).scrollTop($("#divChatLog").prop("scrollHeight"));
	}
	
	function acisionSDK_onMessage(msg) {
	  console.log("acisionSDK_onMessage()");
  	  console.info("Received message " + msg.content);

	  // Force a reload of the audio otherwise Chrome will only ever play the file once
	  document.getElementById("audDing").load();
	  document.getElementById("audDing").play();
	  
	  if (msg.from.indexOf(agent1.username) > -1) {
		  addChatLine(agent1.name, msg.content);
	  } else if (msg.from.indexOf(agent2.username) > -1) {
		  addChatLine(agent2.name, msg.content);	  
	  }
	}	
	
	function onAuthFailure(evt){
		console.log('acision forge authentication error');				
	};	
	
    function isValidPreChat(name, email, phone) {
    	return (name != null && name !== '' && 
    		email != null && email !== '' && 
    		phone != null && phone !== '');
    }

	window.onunload = window.onbeforeonload = (function(){		
		sdk.presence.deleteOwnPresentity();	
		disconnect();
	    $(".ui-dialog-content").dialog("close");
	});		
	
	$(window).resize(function() {
		$("#dlgPreChat").dialog( "option", "position", { my: "center", at: "center", of: window });
		$("#dlgChat").dialog( "option", "position", { my: "center", at: "center", of: window });
	});		

  $("#customerEmail").keypress(handleEnter(dlgPreChat_click));
  $("#txtMessage").keypress(handleEnter(btnSend_click));	
  $("#btnSend").button().click(btnSend_click);
});