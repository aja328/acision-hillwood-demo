function setupAppointmentBehaviors() {
	//agent direct connect
	$('.appointment').click(function(evt){		
		//prevent default
		evt.preventDefault();
				
		//modal
		agent_pin_number = $('#enter-agent-pin').bPopup({ positionStyle: 'fixed'});			
	});
	
	//direct agent pin call
	$('#contact-form').submit(function(evt){
		evt.preventDefault();
		//variables
		agent = $('#contact-form-name').val();
		//hide non valid agent
		$('#non-valid-agent-msg').hide();		
				
		//check if not empty
		if(agent != '' || agent != null){
			if(agent == '1111' || agent == '2222'){
				//hide invalid message
				$('#non-valid-agent-msg').hide();

				//amanda
				if(agent == '1111'){									
					//agent one / username from Forge Acision
					agent = agent1;	
				};
							
				//barry
				if(agent == '2222'){
					//agent two / username from Forge Acision
					agent = agent2;	
				};
					
				//connect to agent
				session = sdk.webrtc.connect(agent, {
					onConnect		: onCallConnectPin,
					onConnecting	: onCallConnectingPin,
					onClose			: onCallClosePin
				},{
					streamConfig: {
					  audioIn	: true,
					  audioOut	: true,
					  videoIn	: true,
					  videoOut	: true
					}
				});	
								
				//local video render
				session.localVideoElement = document.getElementById('videoLocal'); 
				//remote video & audio render
				session.remoteAudioElement 	= document.getElementById('audioRemote');
				session.remoteVideoElement 	= document.getElementById('videoRemote');
				$('video').prop('autoplay', true);						
			} else{
				$('#non-valid-agent-msg').fadeIn();								
			};		
		};			
	});	
}

function onCallConnectingPin(){		
	//notify user that the call is trying to connect
	$('#enter-agent-pin .loading').fadeIn();
};

function onCallClosePin(evt){		
	//get on call close status
	//https://docs.sdk.acision.com/api/latest/javascript/module-WebRTC-Session.html#CloseEvent
	// normal - the session ended normally (including timed out).
	// blocked - the remote party has rejected the session because the local user is blocked.
	// offline - the remote party is offline or wants to appear offline.
	// notfound - the remote party does not exist or wants to appear to not exist.
	var status = evt.status;	
							
	//clean remote html element
	$('#videoLocal').attr('src', '');	
	$('#videoRemote').attr('src', '');
			
	//show waiting messaging
	$('.video .loading').show();
	//hide agent app
	$('.video-chat').hide();
	//show demo app page	
	$('#page-container').fadeIn();
	//close modal
	agent_pin_number.close();
			
	//offline agent
	if(status == 'offline'){
		//show modal
		agent_notice = $('#agent-offline').bPopup();					
		//hide loading
		$('#enter-agent-pin .loading').hide();						
	};			
};	

function onCallConnectPin(){			
	//notify user that the call has been accepted and will start shortly
	$('#enter-agent-pin .loading').hide();
	//close modal
	agent_pin_number.close();
	//hide demo app page
	$('#page-container').hide();
	//show agent app
	$('.video-chat').fadeIn();		
};
